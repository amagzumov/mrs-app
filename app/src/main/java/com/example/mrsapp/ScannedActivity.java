package com.example.mrsapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrsapp.TaskInspector.task_inspector;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class ScannedActivity extends AppCompatActivity {

    String id, meter_id, curr_date, wkeeper_nm, inspector_nm, type, typeEGW, region, tp_id = "", tp_spinner_selected_item, tp_id_mp, tp_code;
    EditText first, last;
    TextView consumer_text, install_place_text, phone_text, account_no_text, inspector_text, wkeeper_text, date_given, date_registered, tp_text, district_text, saved_text, registered_text, meter_id_edit;
    ImageButton save, btn_share;
    LinearLayout info;
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<String> tp_list = new ArrayList<String>();
    ArrayList<String> tp_list_with_id = new ArrayList<String>();
    AutoCompleteTextView tp_edit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanned);

        save = findViewById(R.id.save);
        btn_share = (ImageButton)findViewById(R.id.share);

        meter_id_edit = findViewById(R.id.meter_id_edit);
        first = findViewById(R.id.first_value_edit);
        last = findViewById(R.id.last_value_edit);
        consumer_text = findViewById(R.id.consumer_edit);
        install_place_text = findViewById(R.id.ins_place_edit);
        phone_text = findViewById(R.id.phone_edit);
        account_no_text = findViewById(R.id.licevoy_edit);
        inspector_text = findViewById(R.id.inspector_edit);
        info = (LinearLayout)findViewById(R.id.info);
        date_given = findViewById(R.id.date_saved_edit);
        tp_edit = findViewById(R.id.tp_edit);



        //new GetAllTP(ScannedActivity.this).execute();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        curr_date = dateFormat.format(date);

        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
        type = sharedPreferences2.getString("type", null);
        inspector_nm = sharedPreferences2.getString("fio",null);
        typeEGW = sharedPreferences2.getString("typeEGW", "");
        region = sharedPreferences2.getString("region", "");

        new GetAllTP(ScannedActivity.this).execute();
        new GetAllTPWithId().execute();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2",Context.MODE_PRIVATE);
        id = sharedPreferences.getString("id",null);
        meter_id = sharedPreferences.getString("scanned_meter_id",null);

        meter_id_edit.setText(meter_id);
        if(type.equals("warehouse")){
            new GetFirstOneRecordWarehouse(ScannedActivity.this).execute();
        }else if(type.equals("inspector")){
            new GetRecordFromMeteringPoint(ScannedActivity.this).execute();
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("warehouse")){
                    new SaveRecordWarehouse(ScannedActivity.this).execute();
                }else if(type.equals("inspector")){
                    new RecordToMetersWithUpdatingWarehouseMetersReservedStatus(ScannedActivity.this).execute();
                }
            }
        });

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, getImageUri(ScannedActivity.this, getBitmapFromView(info)));
                try {
                    startActivity(Intent.createChooser(i, "Share via..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });

        tp_edit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                final Spinner tp_spinner = new Spinner(ScannedActivity.this);

                //if(!tp_list.isEmpty()){
                    String[] fruits = {"Apple", "Banana", "Cherry", "Date", "Grape", "Kiwi", "Mango", "Pear"};
                    ArrayAdapter adapter = new ArrayAdapter(ScannedActivity.this, android.R.layout.simple_spinner_item, tp_list);
                    adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);//select_dialog_singlechoice
                    /*Spinner spinner = (Spinner) findViewById(R.id.spinner2);*/
                    tp_spinner.setAdapter(adapter);
                    //tp_spinner.setPrompt("Title");
                    tp_spinner.setSelection(0);
                    tp_spinner_selected_item = "";

                tp_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        tp_spinner_selected_item = tp_spinner.getSelectedItem().toString();
                        String ids = FindIDByTPName(tp_spinner_selected_item);
                        Toast.makeText(getApplicationContext(), ids, Toast.LENGTH_LONG).show();
                        tp_spinner_selected_item = ids;
                        tp_id = ids;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        tp_id = "";
                    }
                });
                /*if(tp_spinner.getSelectedItemPosition() == 0){

                }else{
                    tp_spinner_selected_item = tp_spinner.getSelectedItem().toString();

                }*/

                    new AlertDialog.Builder(ScannedActivity.this)
                            .setTitle("Изменить ТП")
                            .setMessage("Выберите ТП из списка")
                            .setView(tp_spinner, 50, 0 ,0 ,0)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if(tp_spinner_selected_item.equals("")){

                                        return;
                                    }else{
                                        tp_edit.setText(tp_spinner.getSelectedItem().toString());
                                    }

                                    Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
                                }
                            })
                            .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    tp_spinner_selected_item = "";
                                    //Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
                                    //dialog.cancel();
                                }
                            })
                            .show();



                //}


            }
        });

    }

    //region ShareVIA
    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(),      view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    //endregion

    public class GetFirstOneRecordWarehouse extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public GetFirstOneRecordWarehouse(ScannedActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetFirstOneRecordWarehouse(id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
            }else{
                ArrayList<String> list = new ArrayList<String>();
                try {
                    list = parseArray(result);

                    consumer_text.setText(list.get(0).toString());
                    install_place_text.setText(list.get(1).toString());
                    phone_text.setText(list.get(2).toString());
                    account_no_text.setText(list.get(3).toString());
                    inspector_text.setText(inspector_nm);
                    wkeeper_text.setText(wkeeper_nm);
                    date_given.setText(curr_date);
                    first.setText("");
                    first.setText("");
                    tp_text.setText(list.get(4).toString());
                    district_text.setText(list.get(5).toString());
                    if(list.get(6).toString().contains("yes")){
                        saved_text.setTextColor(Color.GREEN);
                        saved_text.setText("Сохранен");
                    }else if(list.get(6).toString().contains("no")){
                        saved_text.setTextColor(Color.RED);
                        saved_text.setText("Не сохранен");
                    }

                    if(list.get(7).toString().contains("yes")){
                        registered_text.setText("Зарегистрирован");
                        registered_text.setTextColor(Color.GREEN);

                    }else if(list.get(7).toString().contains("no")){
                        registered_text.setText("Не зарегистрирован");
                        registered_text.setTextColor(Color.RED);
                    }
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                } catch (ParserConfigurationException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (SAXException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (XPathExpressionException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public class GetAllTP extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public GetAllTP(ScannedActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetAllTP(region);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            try {
                tp_list = parseArray(result);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            tp_list.add(0, "Выберите ТП...");
            /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(ScannedActivity.this, android.R.layout.simple_spinner_item, tp_list);
            adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);//select_dialog_singlechoice
            /*Spinner spinner = (Spinner) findViewById(R.id.spinner2);
            tp_spinner.setAdapter(adapter);
            tp_spinner.setPrompt("Title");
            tp_spinner.setSelection(0);*/
            /*ArrayAdapter<String> adapter_tp_list = new ArrayAdapter<String>
                    (getApplicationContext(),android.R.layout.select_dialog_item,tp_list);
            adapter_tp_list.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
            tp_edit.setThreshold(3);
            tp_edit.setAdapter(adapter_tp_list);*/
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    public class GetAllTPWithId extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetAllTPWithId(region);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            try {

                tp_list_with_id = parseArray(result);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }

            /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(ScannedActivity.this, android.R.layout.simple_spinner_item, tp_list);
            adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);//select_dialog_singlechoice
            /*Spinner spinner = (Spinner) findViewById(R.id.spinner2);
            tp_spinner.setAdapter(adapter);
            tp_spinner.setPrompt("Title");
            tp_spinner.setSelection(0);*/
            /*ArrayAdapter<String> adapter_tp_list = new ArrayAdapter<String>
                    (getApplicationContext(),android.R.layout.select_dialog_item,tp_list);
            adapter_tp_list.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
            tp_edit.setThreshold(3);
            tp_edit.setAdapter(adapter_tp_list);*/
        }
    }

    public class GetFirstOneRecordInspector extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public GetFirstOneRecordInspector(ScannedActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetFirstOneRecordInspector(meter_id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
            }else{
                ArrayList<String> list = new ArrayList<String>();
                try {
                    list = parseArray(result);

                    consumer_text.setText(list.get(0).toString());
                    install_place_text.setText(list.get(1).toString());
                    phone_text.setText(list.get(2).toString());
                    first.setText(list.get(9).replace(",", ".").toString());
                    last.setText(list.get(10).replace(",", ".").toString());
                    account_no_text.setText(list.get(3).toString());
                    inspector_text.setText(list.get(7).toString());
                    wkeeper_text.setText(list.get(8).toString());
                    date_given.setText(list.get(6).toString());
                    date_registered.setText(curr_date);
                    tp_text.setText(list.get(4).toString());
                    district_text.setText(list.get(5).toString());
                    if(list.get(11).toString().contains("yes")){
                        saved_text.setTextColor(Color.GREEN);
                        saved_text.setText("Сохранен");
                    }else if(list.get(11).toString().contains("no")){
                        saved_text.setTextColor(Color.RED);
                        saved_text.setText("Не сохранен");
                    }

                    if(list.get(12).toString().contains("yes")){
                        registered_text.setText("Зарегистрирован");
                        registered_text.setTextColor(Color.GREEN);

                    }else if(list.get(12).toString().contains("no")){
                        registered_text.setText("Не зарегистрирован");
                        registered_text.setTextColor(Color.RED);
                    }
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                } catch (ParserConfigurationException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (SAXException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (XPathExpressionException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public class SaveRecordWarehouse extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public SaveRecordWarehouse(ScannedActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Сохранение данных...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String first_p = (first.getText().toString().equals("")) ? "0.00" : first.getText().toString();
            String last_p = (last.getText().toString().equals("")) ? "0.00" : last.getText().toString();
            String response = cs.SaveRecordWarehouse(meter_id, first_p, last_p, wkeeper_nm, curr_date, id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
            }else if(result.contains("success")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_LONG).show();
                startActivity(new Intent(ScannedActivity.this, MRSActivityWarehouse.class));
            }
        }
    }

    public class RecordToMetersWithUpdatingWarehouseMetersReservedStatus extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public RecordToMetersWithUpdatingWarehouseMetersReservedStatus(ScannedActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Сохранение данных...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String first_p = (first.getText().toString().equals("")) ? "0.00" : first.getText().toString();
            String last_p = (last.getText().toString().equals("")) ? "0.00" : last.getText().toString();
            String tp = (tp_edit.getText().toString());
            String tp_new;
            if(tp_id.equals("")){
               tp_new = tp_id_mp;
            }else{
                tp_new = tp_id;
            }
            String response = cs.RecordToMetersWithUpdatingWarehouseMetersReservedStatus(list.get(2), meter_id, list.get(0), list.get(3), list.get(4), list.get(1), first_p, last_p, inspector_nm, typeEGW, curr_date, id, tp, region, tp_new, tp_code);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
            }else if(result.contains("success")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if(type.equals("warehouse")){
                    Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ScannedActivity.this, MRSActivityWarehouse.class));
                }else if(type.equals("inspector")){
                    Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ScannedActivity.this, MRSActivityInspector.class));
                }

            }else if(result.contains("unsuccess")){
                Toast.makeText(getApplicationContext(), "Ошиибка сохранения", Toast.LENGTH_LONG).show();
            }
        }
    }

    /*public class RecordToMetersWithUpdatingWarehouseMetersReservedStatus extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.RecordToWarehouseMeters(result_meter_id, fName, curentDate);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("success")){
                Toast.makeText(ScannedActivity.this, "Счетчик присвоен абоненту", Toast.LENGTH_SHORT).show();
            }else if(result.contains("unsuccess")){
                Toast.makeText(ScannedActivity.this, "Не удалось присвоить счетчик абоненту", Toast.LENGTH_SHORT).show();
            }else if(result.contains("Failed to connect")){
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }
        }
    }*/

    public class GetRecordFromMeteringPoint extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public GetRecordFromMeteringPoint(ScannedActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetRecordFromMeteringPoint(id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
            }else{

                try {
                    list = parseArray(result);
                    consumer_text.setText(list.get(0).toString());
                    install_place_text.setText(list.get(3).toString());
                    phone_text.setText(list.get(4).toString());
                    account_no_text.setText(list.get(1).toString());
                    inspector_text.setText(inspector_nm);
                    date_given.setText(curr_date);
                    tp_edit.setText(list.get(5));
                    tp_id_mp = list.get(6);
                    tp_code = list.get(7);
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    } catch (ParserConfigurationException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (SAXException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                } catch (XPathExpressionException e) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    /*private ArrayList<task_inspector> parseList(String tp) {
        int i = 0;
        ArrayList<String> draft = new ArrayList<>();
        try {
            draft = parseArray(tp);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        for (i=0;i<draft.size();i++){
            String[] tokens = split_new(draft.get(i),"|");
            String install_place = tokens[0];
            String consumer = tokens[1];
            String id = tokens[2];
        }
        return products;
    }*/

    public static String[] split_new(String original, String separator)
    {
        ArrayList<String> nodes = new ArrayList<>();
        int index = original.indexOf(separator);
        while (index >= 0)
        {
            nodes.add(original.substring(0, index));
            original = original.substring(index + separator.length());
            index = original.indexOf(separator);
        }
        nodes.add(original);
        return nodes.toArray(new String[nodes.size()]);
    }

    private ArrayList<String> parseArray(String tp) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(tp.getBytes()));
        final XPathExpression xPathExpression = XPathFactory.newInstance().newXPath().compile("//string/text()");
        final NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
        final ArrayList<String> tp_list = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            tp_list.add(nodeList.item(i).getNodeValue());
        }
        return tp_list;
    }

    private String FindIDByTPName(String tp_name){
        String result = "";
        int searchListLength = tp_list_with_id.size();
        for (int i = 0; i < searchListLength; i++) {
            if (tp_list_with_id.get(i).contains(tp_name)) {
                StringTokenizer tokens = new StringTokenizer(tp_list_with_id.get(i), "|");
                String tp = tokens.nextToken();// type
                String tp_id = tokens.nextToken();// name and surname
                result = tp_id;
            }
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
        startActivity(intent);
        finish();
    }

}
