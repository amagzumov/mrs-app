package com.example.mrsapp;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity {

    CardView cv;
    EditText us, pw;
    public static final String LOGIN = "Login";
    private static final String LOGIN_USERNAME = "login";
    private static final String LOGIN_PASSWORD = "pass";
    private static final int REQUEST = 112;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cv = (CardView)findViewById(R.id.cardViewLogin);
        us = findViewById(R.id.editText);
        pw = findViewById(R.id.editText2);

        SharedPreferences pref = getSharedPreferences(LOGIN,MODE_PRIVATE);
        String username = pref.getString(LOGIN_USERNAME, null);
        String password = pref.getString(LOGIN_PASSWORD, null);

        if (username != null || password != null) {
            us.setText(username);
            pw.setText(password);
        }

        String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_WIFI_STATE};
        if (!hasPermissions(MainActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions((Activity) MainActivity.this, PERMISSIONS, REQUEST );
        }

        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AssyncCallSoap(MainActivity.this).execute();
            }
        });
    }

    public class AssyncCallSoap extends AsyncTask<String, String, String>{
        private ProgressDialog dialog;

        public AssyncCallSoap(MainActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Авторизация...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.Login(us.getText().toString(), pw.getText().toString(), "login_java");
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("<login_javaResult>")){
                final Pattern pattern = Pattern.compile("<login_javaResult>(.+?)</login_javaResult>");
                final Matcher matcher = pattern.matcher(result);
                matcher.find();
                String res = matcher.group(1);
                if(res.equals("no")){
                    Toast.makeText(getApplicationContext(), "Неверный логин или пароль", Toast.LENGTH_LONG).show();
                }else{
                    if(res.contains("|")) {
                        StringTokenizer tokens = new StringTokenizer(res, "|");
                        String type = tokens.nextToken();// type
                        String fio = tokens.nextToken();// name and surname
                        String region = tokens.nextToken();
                        String typeEGW = tokens.nextToken();
                        if(type.equals("warehouse")){
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
                            sharedPreferences.edit().putString("fio",fio).apply();
                            sharedPreferences.edit().putString("type",type).apply();
                            //sharedPreferences.edit().putString("region",region).apply();
                            getSharedPreferences(LOGIN,MODE_PRIVATE)
                                    .edit()
                                    .putString(LOGIN_USERNAME, us.getText().toString())
                                    .putString(LOGIN_PASSWORD, pw.getText().toString())
                                    .commit();
                            new RecordMobileLog().execute();
                            Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(MainActivity.this, MRSActivityWarehouse.class));
                        }else if(type.equals("inspector")){
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
                            sharedPreferences.edit().putString("fio",fio).apply();
                            sharedPreferences.edit().putString("type",type).apply();
                            sharedPreferences.edit().putString("region",region).apply();
                            sharedPreferences.edit().putString("typeEGW",typeEGW).apply();
                            getSharedPreferences(LOGIN,MODE_PRIVATE)
                                    .edit()
                                    .putString(LOGIN_USERNAME, us.getText().toString())
                                    .putString(LOGIN_PASSWORD, pw.getText().toString())
                                    .commit();
                            new RecordMobileLog().execute();
                            Intent intent = new Intent(MainActivity.this, MRSActivityInspector.class);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }else if(result.contains("Failed to connect")) {
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    public class RecordMobileLog extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
            String fName = sharedPreferences.getString("fio",null);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date date = new Date();
            String curr_date = dateFormat.format(date);
            SimpleDateFormat dateFormattime = new SimpleDateFormat("hh:mm");
            Date time = new Date();
            String curr_time = dateFormattime.format(time);
            String mobileIp = getMobileIPAddress();
            CallSoap cs = new CallSoap();
            String response = cs.RecordMobileLog(fName, "scan", mobileIp, Build.MANUFACTURER + " " + Build.MODEL + " (Android " + Build.VERSION.RELEASE + ")", curr_date, curr_time);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    @Override
    public void onBackPressed() {
        //System.exit(0);
        SharedPreferences insp_tp = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
        insp_tp.edit().remove("inspector_nm").commit();
        insp_tp.edit().remove("tp_this").commit();
        this.finishAffinity();
    }

}
