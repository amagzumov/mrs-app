package com.example.mrsapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrsapp.SearchByDateInspector.search_by_date_inspector;
import com.example.mrsapp.SearchByDateInspector.search_by_date_inspector_adapter;
import com.example.mrsapp.TaskInspector.task_inspector;
import com.example.mrsapp.TaskInspector.task_inspector_adapter;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class MRSActivityInspector extends AppCompatActivity {

    public String fName, spinner_selected_item, region;
    public static String undefined, selected_region;
    TextView tw;
    ImageView online;
    public ListView lv, lv2;
    private int mYear, mMonth, mDay;
    public EditText curentDate;
    ArrayList<search_by_date_inspector> searchByDateInsp = new ArrayList<>();
    ArrayList<task_inspector> products = new ArrayList<task_inspector>();
    com.example.mrsapp.TaskInspector.task_inspector_adapter task_inspector_adapter;
    com.example.mrsapp.SearchByDateInspector.search_by_date_inspector_adapter search_by_date_inspector_adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AutoCompleteTextView searchByPersAccount;
    Spinner spinner;
    ImageButton searchBtn;
    Button AddUndefMP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrsinspector);
        spinner = findViewById(R.id.spinner);
        tw = findViewById(R.id.textView2);
        online = findViewById(R.id.online);
        lv = findViewById(R.id.listview1);
        lv2 = findViewById(R.id.listview2);
        curentDate = findViewById(R.id.date);
        searchByPersAccount = findViewById(R.id.autoCompleteTextView2);
        searchBtn = findViewById(R.id.imageButton);
        AddUndefMP = findViewById(R.id.create_mp);

        //region TabsConfig
        setTitle("TabHost");
        TabHost tabHost = findViewById(R.id.tabHost);
        tabHost.setup();
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Абоненты");
        tabHost.addTab(tabSpec);
        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("История");
        tabHost.addTab(tabSpec);
        tabHost.setCurrentTab(0);
        //endregion

        //region GetUsername
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
        fName = sharedPreferences.getString("fio",null);
        region = sharedPreferences.getString("region", null);
        tw.setText(fName);
        //endregion

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
             @Override
             public void onRefresh() {
                 products.clear();
                 new SearchByPersAccount(MRSActivityInspector.this).execute();
             }
         });

        //region CheckInternetConnectivity
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if(isConnected()){
                        online.setBackgroundResource(R.drawable.green_circle);
                    }else if(!isConnected()){
                        online.setBackgroundResource(R.drawable.red_circle);
                    }
                    handler.postDelayed(this, 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        //Start
        handler.postDelayed(runnable, 1000);
        //endregion


        new GetAllRegions(MRSActivityInspector.this).execute();


        //new GetFirstTenRecordsInspector(MRSActivityInspector.this).execute();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                task_inspector tw = products.get(position);
                String meter_ids = tw.getId();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
                sharedPreferences.edit().putString("id",meter_ids).apply();
                sharedPreferences.edit().putString("wkeeper_nm",fName).apply();
                undefined = "";
                startActivity(new Intent(MRSActivityInspector.this, ScanActivity.class));
            }
        });

        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                search_by_date_inspector tw = searchByDateInsp.get(position);
                String meter_id = tw.getMeter_id2();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
                sharedPreferences.edit().putString("meter_id",meter_id).apply();
                startActivity(new Intent(MRSActivityInspector.this, InfoViewActivity.class));
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                products.clear();
                new SearchByPersAccount(MRSActivityInspector.this).execute();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinner_selected_item = spinner.getSelectedItem().toString();
                products.clear();
                new SearchByPersAccount(MRSActivityInspector.this).execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        AddUndefMP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spinner.getSelectedItemPosition() == 0){
                    Toast.makeText(getApplicationContext(), "Выберите район", Toast.LENGTH_LONG).show();
                }else{
                    selected_region = spinner.getSelectedItem().toString();
                    undefined = "undefined";
                    startActivity(new Intent(MRSActivityInspector.this, ScanActivity.class));
                }

            }
        });
    }

    public boolean isConnected() throws InterruptedException, IOException {
        final String command = "ping -c 1 google.com";
        return Runtime.getRuntime().exec(command).waitFor() == 0;
    }

    public class SearchByPersAccount extends AsyncTask<String, String, String> {

        private ProgressDialog dialog;


        public SearchByPersAccount(MRSActivityInspector activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.SearchByPersAccount(searchByPersAccount.getText().toString(), spinner_selected_item);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }else{
                parseList(result);
                task_inspector_adapter = new task_inspector_adapter(MRSActivityInspector.this, products);
                lv.setAdapter(task_inspector_adapter);
                if (dialog.isShowing()) dialog.dismiss();
            }
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private ArrayList<task_inspector> parseList(String tp) {
        int i = 0;
        ArrayList<String> draft = new ArrayList<>();
        try {
            draft = parseArray(tp);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        for (i=0;i<draft.size();i++){
            String[] tokens = split_new(draft.get(i),"|");
            String install_place = tokens[0];
            String consumer = tokens[1];
            String id = tokens[2];
            String pers_account = tokens[3];
            products.add(new task_inspector(pers_account, install_place, consumer, id));
        }
        return products;
    }

    public static String[] split_new(String original, String separator)
    {
        ArrayList<String> nodes = new ArrayList<>();
        int index = original.indexOf(separator);
        while (index >= 0)
        {
            nodes.add(original.substring(0, index));
            original = original.substring(index + separator.length());
            index = original.indexOf(separator);
        }
        nodes.add(original);
        return nodes.toArray(new String[nodes.size()]);
    }

    private ArrayList<String> parseArray(String tp) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(tp.getBytes()));
        final XPathExpression xPathExpression = XPathFactory.newInstance().newXPath().compile("//string/text()");
        final NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
        final ArrayList<String> tp_list = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            tp_list.add(nodeList.item(i).getNodeValue());
        }
        return tp_list;
    }

    public void setDateInspector(View v) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if(monthOfYear < 10 && dayOfMonth < 10){
                            searchByDateInsp.clear();
                            curentDate.setText("0" + dayOfMonth + ".0" + (monthOfYear + 1) + "." + year);
                            new SearchRecordsByDateInspector(MRSActivityInspector.this).execute();
                        }else if(monthOfYear < 10 && dayOfMonth >= 10){
                            searchByDateInsp.clear();
                            curentDate.setText(dayOfMonth + ".0" + (monthOfYear + 1) + "." + year);
                            new SearchRecordsByDateInspector(MRSActivityInspector.this).execute();
                        }else if(monthOfYear >= 10 && dayOfMonth < 10){
                            searchByDateInsp.clear();
                            curentDate.setText("0" + dayOfMonth + "." + (monthOfYear + 1) + "." + year);
                            new SearchRecordsByDateInspector(MRSActivityInspector.this).execute();
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public class SearchRecordsByDateInspector extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public SearchRecordsByDateInspector(MRSActivityInspector activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String date = curentDate.getText().toString();
            CallSoap cs = new CallSoap();
            String response2 = cs.SearchRecordsByDateInspector(date, fName);
            return response2;
        }

        @Override
        protected void onPostExecute(String result2){
            super.onPostExecute(result2);
            parseListSearch(result2);
            if(result2.contains("Failed to connect")) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }else{
                search_by_date_inspector_adapter = new search_by_date_inspector_adapter(MRSActivityInspector.this, searchByDateInsp);
                lv2.setAdapter(search_by_date_inspector_adapter);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }

    public class GetAllRegions extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public GetAllRegions(MRSActivityInspector activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String date = curentDate.getText().toString();
            CallSoap cs = new CallSoap();
            String response2 = cs.GetAllRegions(region);
            return response2;
        }

        @Override
        protected void onPostExecute(String result2){
            super.onPostExecute(result2);
            //parseListSearch(result2);
            if(result2.contains("Failed to connect")) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }else{
                ArrayList regionsList = null;
                try {
                    regionsList = parseArray(result2);
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (XPathExpressionException e) {
                    e.printStackTrace();
                }
                regionsList.add(0, "Выберите район...");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MRSActivityInspector.this, android.R.layout.simple_spinner_item, regionsList);
                adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                Spinner spinner = (Spinner) findViewById(R.id.spinner);
                spinner.setAdapter(adapter);
                spinner.setPrompt("Title");
                spinner.setSelection(0);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }

    private ArrayList<search_by_date_inspector> parseListSearch(String tp){
        int i = 0;
        ArrayList<String> draft = new ArrayList<>();
        try {
            draft = parseArray(tp);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        for (i=0;i<draft.size();i++){
            StringTokenizer tokens = new StringTokenizer(draft.get(i), "|");
            //String tp_name = tokens.nextToken();// type
            String consumer = tokens.nextToken();// name and surname
            String meter_id = tokens.nextToken();// type
            String date = tokens.nextToken();
            searchByDateInsp.add(new search_by_date_inspector(meter_id, consumer, "" , date ));
        }
        return searchByDateInsp;
    }

    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }



    /*@Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("date",Context.MODE_PRIVATE);
        String date = sharedPreferences.getString("date",null);
        if(date.equals("") && date.isEmpty()){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date date2 = new Date();
            curentDate.setText(dateFormat.format(date2));
        }else{
            curentDate.setText(date);
        }
        new SearchRecordsByDateInspector(MRSActivityInspector.this).execute();
        Toast.makeText(getApplicationContext(), "ONRESUME", Toast.LENGTH_LONG).show();
        sharedPreferences.edit().clear().commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        Toast.makeText(getApplicationContext(), "ONSTOP", Toast.LENGTH_LONG).show();
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("date", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("date",curentDate.getText().toString()).apply();
    }*/


}
