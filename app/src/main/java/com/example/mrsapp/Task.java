package com.example.mrsapp;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.util.TimerTask;

public class Task extends TimerTask{

    Activity context;

    public void run() {
        if(haveNetworkConnection()){
            Toast toast = Toast.makeText(context.getApplicationContext(),"CONNECTED", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            Toast toast = Toast.makeText(context.getApplicationContext(),"DISCONNECTED", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
