package com.example.mrsapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

public class CallSoap{

    public String Login(String username, String password, String task){
        String SOAP_ACTION = "http://213.230.97.64/" + task;
        String OPERATION_NAME = task;//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        if(task == "login_java"){
            PropertyInfo PI = new PropertyInfo();
            PI.setName("login");
            PI.setValue(username);
            PI.setType(String.class);
            request.addProperty(PI);

            PI = new PropertyInfo();
            PI.setName("password");
            PI.setValue(password);
            PI.setType(String.class);
            request.addProperty(PI);
        }

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetAllTP(String region){
        String SOAP_ACTION = "http://213.230.97.64/GetAllTP";
        String OPERATION_NAME = "GetAllTP";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("id");
        PI.setValue(region);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetAllTPWithId(String region){
        String SOAP_ACTION = "http://213.230.97.64/GetAllTPWithId";
        String OPERATION_NAME = "GetAllTPWithId";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("id");
        PI.setValue(region);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetAllInspectors(){
        String SOAP_ACTION = "http://213.230.97.64/GetAllInspectors";
        String OPERATION_NAME = "GetAllInspectors";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetFirstTenRecordsWarehouse(String group_nm, String inspector_nm){
        String SOAP_ACTION = "http://213.230.97.64/GetFirstTenRecordsWarehouse";
        String OPERATION_NAME = "GetFirstTenRecordsWarehouse";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("group_nm");
        PI.setValue(group_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("inspector_nm");
        PI.setValue(inspector_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String SearchRecordsByDateWarehouse(String date){
        String SOAP_ACTION = "http://213.230.97.64/SearchRecordsByDateWarehouse";
        String OPERATION_NAME = "SearchRecordsByDateWarehouse";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("date");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String UserInfo(String stroka){
        String SOAP_ACTION = "http://213.230.97.64/UserInfo";
        String OPERATION_NAME = "UserInfo";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("stroka");
        PI.setValue(stroka);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String CheckMeterWarehouse(String meter_id){
        String SOAP_ACTION = "http://213.230.97.64/CheckMeterWarehouse";
        String OPERATION_NAME = "CheckMeterWarehouse";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String CheckMeterInspector(String meter_id){
        String SOAP_ACTION = "http://213.230.97.64/CheckMeterInspector";
        String OPERATION_NAME = "CheckMeterInspector";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetFirstOneRecordInspector(String meter_id){
        String SOAP_ACTION = "http://213.230.97.64/GetFirstOneRecordInspector";
        String OPERATION_NAME = "GetFirstOneRecordInspector";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetFirstOneRecordWarehouse(String id){
        String SOAP_ACTION = "http://213.230.97.64/GetFirstOneRecordWarehouse";
        String OPERATION_NAME = "GetFirstOneRecordWarehouse";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("id");
        PI.setValue(id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String SaveRecordWarehouse(String meter_id, String energy_value_first, String energy_value_last, String wkeeper_nm, String scan_dt, String id){
        String SOAP_ACTION = "http://213.230.97.64/SaveRecordWarehouse";
        String OPERATION_NAME = "SaveRecordWarehouse";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("energy_value_first");
        PI.setValue(energy_value_first);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("energy_value_last");
        PI.setValue(energy_value_last);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("wkeeper_nm");
        PI.setValue(wkeeper_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("scan_dt");
        PI.setValue(scan_dt);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("id");
        PI.setValue(id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String SaveRecordInspector(String meter_id, String energy_value_first, String energy_value_last, String reg_dt){
        String SOAP_ACTION = "http://213.230.97.64/SaveRecordInspector";
        String OPERATION_NAME = "SaveRecordInspector";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("energy_value_first");
        PI.setValue(energy_value_first);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("energy_value_last");
        PI.setValue(energy_value_last);
        PI.setType(String.class);
        request.addProperty(PI);


        PI = new PropertyInfo();
        PI.setName("reg_dt");
        PI.setValue(reg_dt);
        PI.setType(String.class);
        request.addProperty(PI);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetFirstTenRecordsInspector(String inspector_nm){
        String SOAP_ACTION = "http://213.230.97.64/GetFirstTenRecordsInspector";
        String OPERATION_NAME = "GetFirstTenRecordsInspector";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("inspector");
        PI.setValue(inspector_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String SearchRecordsByDateInspector(String date, String inspector_nm){
        String SOAP_ACTION = "http://213.230.97.64/SearchRecordsByDateInspector";
        String OPERATION_NAME = "SearchRecordsByDateInspector";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("date");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("inspector_nm");
        PI.setValue(inspector_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String RecordToWarehouseMeters(String meter_id, String wkeeper_nm, String date){
        String SOAP_ACTION = "http://213.230.97.64/RecordToWarehouseMeters";
        String OPERATION_NAME = "RecordToWarehouseMeters";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("wkeeper_nm");
        PI.setValue(wkeeper_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("date");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String CheckInWarehouseMeters(String meter_id){
        String SOAP_ACTION = "http://213.230.97.64/CheckInWarehouseMeters";
        String OPERATION_NAME = "CheckInWarehouseMeters";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String CheckInMetersUndefMeterPoint(String meter_id){
        String SOAP_ACTION = "http://213.230.97.64/CheckInMetersUndefMeterPoint";
        String OPERATION_NAME = "CheckInMetersUndefMeterPoint";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String SearchByPersAccount(String data, String region){
        String SOAP_ACTION = "http://213.230.97.64/SearchByPersAccount";
        String OPERATION_NAME = "SearchByPersAccount";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("data");
        PI.setValue(data);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("region");
        PI.setValue(region);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String CheckInMeterExistAndReserved(String meter_id){
        String SOAP_ACTION = "http://213.230.97.64/CheckInMeterExistAndReserved";
        String OPERATION_NAME = "CheckInMeterExistAndReserved";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetRecordFromMeteringPoint(String id){
        String SOAP_ACTION = "http://213.230.97.64/GetRecordFromMeteringPoint";
        String OPERATION_NAME = "GetRecordFromMeteringPoint";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("id");
        PI.setValue(id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String RecordToMetersWithUpdatingWarehouseMetersReservedStatus(String dcu_id, String meter_id, String consumer, String install_place, String phone, String pers_account, String energy_value_first, String energy_value_last, String inspector_nm, String meter_type, String date, String parent_id, String tp, String region, String tp_id, String tp_code){
        String SOAP_ACTION = "http://213.230.97.64/RecordToMetersWithUpdatingWarehouseMetersReservedStatus";
        String OPERATION_NAME = "RecordToMetersWithUpdatingWarehouseMetersReservedStatus";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("dcu_id");
        PI.setValue(dcu_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("consumer");
        PI.setValue(consumer);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("install_place");
        PI.setValue(install_place);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("phone");
        PI.setValue(phone);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("pers_account");
        PI.setValue(pers_account);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("install_place");
        PI.setValue(install_place);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("energy_value_first");
        PI.setValue(energy_value_first);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("energy_value_last");
        PI.setValue(energy_value_last);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("meter_type");
        PI.setValue(meter_type);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("scan_dt");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("parent_id");
        PI.setValue(parent_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("inspector_nm");
        PI.setValue(inspector_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("tp");
        PI.setValue(tp);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("region");
        PI.setValue(region);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("tp_id");
        PI.setValue(tp_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("tp_code");
        PI.setValue(tp_code);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String RecordToUndefinedMeteringPoint(String meter_id, String description, String date, String inspector_nm, String region){
        String SOAP_ACTION = "http://213.230.97.64/RecordToUndefinedMeteringPoint";
        String OPERATION_NAME = "RecordToUndefinedMeteringPoint";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("meter_id");
        PI.setValue(meter_id);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("description");
        PI.setValue(description);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("date");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("inspector_nm");
        PI.setValue(inspector_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("region");
        PI.setValue(region);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String GetAllRegions(String region){
        String SOAP_ACTION = "http://213.230.97.64/GetAllRegions";
        String OPERATION_NAME = "GetAllRegions";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("parent_id");
        PI.setValue(region);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }

    public String RecordMobileLog(String username, String operation, String ip_address, String machine_nm, String date, String time){
        String SOAP_ACTION = "http://213.230.97.64/RecordMobileLog";
        String OPERATION_NAME = "RecordMobileLog";//"login_java"
        String WSDL_TARGET_NAMESPACE = "http://213.230.97.64/";
        String SOAP_ADDRESS = "http://213.230.97.64:6010/MobileServiceApp/WebService1.asmx";

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("username");
        PI.setValue(username);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("operation");
        PI.setValue(operation);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("ip_address");
        PI.setValue(ip_address);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("machine_nm");
        PI.setValue(machine_nm);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("date");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("time");
        PI.setValue(time);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        String response = null;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            response = httpTransport.responseDump;
        }catch (Exception ex){
            response = ex.getMessage();
        }

        return response;
    }


}
