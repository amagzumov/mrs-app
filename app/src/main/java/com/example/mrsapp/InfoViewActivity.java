package com.example.mrsapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class InfoViewActivity extends AppCompatActivity {

    ImageButton btn_share;
    LinearLayout info;
    TextView meter_id, consumer, install_place, phone, pers_account, energy_first, energy_last, inspector, wkeeper, scan_dt, reg_dt, tp, district, saved, registered;
    String m_id, type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_view);

        btn_share = (ImageButton)findViewById(R.id.imageButton2);
        meter_id = (TextView)findViewById(R.id.meter_id_edit);
        consumer= (TextView)findViewById(R.id.consumer_edit);
        install_place = (TextView)findViewById(R.id.ins_place_edit);
        phone = (TextView)findViewById(R.id.phone_edit);
        pers_account = (TextView)findViewById(R.id.licevoy_edit);
        energy_first = (TextView)findViewById(R.id.first_value_edit);
        energy_last = (TextView)findViewById(R.id.last_value_edit);
        inspector = (TextView)findViewById(R.id.inspector_edit);
        wkeeper = (TextView)findViewById(R.id.warehouse_edit);
        scan_dt = (TextView)findViewById(R.id.date_saved_edit);
        reg_dt = (TextView)findViewById(R.id.date_reg_edit);
        tp = (TextView)findViewById(R.id.tp_edit);
        district = (TextView)findViewById(R.id.dist_edit);
        saved = (TextView)findViewById(R.id.saved_edit);
        registered = (TextView)findViewById(R.id.registered_edit);


        info = (LinearLayout)findViewById(R.id.info);

        new UserInfo().execute();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2",Context.MODE_PRIVATE);
        m_id = sharedPreferences.getString("meter_id",null);
        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
        type = sharedPreferences2.getString("type", null);

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, getImageUri(InfoViewActivity.this, getBitmapFromView(info)));
                try {
                    startActivity(Intent.createChooser(i, "Share via"));
                } catch (android.content.ActivityNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(),      view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public class UserInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.UserInfo(m_id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            ArrayList warehouse_records = new ArrayList();
            try {
                warehouse_records = parseArray(result);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }

            meter_id.setText(warehouse_records.get(0).toString());
            consumer.setText(warehouse_records.get(1).toString());
            install_place.setText(warehouse_records.get(2).toString());
            phone.setText(warehouse_records.get(3).toString());
            pers_account.setText(warehouse_records.get(4).toString());
            energy_first.setText(warehouse_records.get(5).toString());
            energy_last.setText(warehouse_records.get(6).toString());
            inspector.setText(warehouse_records.get(7).toString());
            wkeeper.setText(warehouse_records.get(8).toString());
            scan_dt.setText(warehouse_records.get(9).toString());
            reg_dt.setText(warehouse_records.get(10).toString());
            //tp.setText(warehouse_records.get(11).toString());
            //district.setText(warehouse_records.get(12).toString());
            if(warehouse_records.get(11).toString().contains("yes")){
                saved.setTextColor(Color.parseColor("#008000"));
                saved.setText("Сохранен");
            }else if(warehouse_records.get(11).toString().contains("no")){
                saved.setTextColor(Color.parseColor("#008000"));
                saved.setText("Не сохранен");
            }

            if(warehouse_records.get(12).toString().contains("yes")){
                registered.setText("Зарегистрирован");
                registered.setTextColor(Color.GREEN);

            }else if(warehouse_records.get(12).toString().contains("no")){
                registered.setText("Не зарегистрирован");
                registered.setTextColor(Color.RED);

            }
        }
    }

    private ArrayList<String> parseArray(String tp) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(tp.getBytes()));
        final XPathExpression xPathExpression = XPathFactory.newInstance().newXPath().compile("//string/text()");
        final NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
        final ArrayList<String> tp_list = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            tp_list.add(nodeList.item(i).getNodeValue());
        }
        return tp_list;
    }

    @Override
    public void onBackPressed() {
        if(type.equals("warehouse")){
            Intent intent = new Intent(getApplicationContext(), MRSActivityWarehouse.class);
            startActivity(intent);
            finish();
        }else if(type.equals("inspector")) {
            Intent intent = new Intent(getApplicationContext(), MRSActivityInspector.class);
            startActivity(intent);
            finish();
        }

    }


}
