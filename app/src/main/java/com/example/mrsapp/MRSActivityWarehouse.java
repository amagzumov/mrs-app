package com.example.mrsapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrsapp.SearchByDateWarehouse.search_by_date_warehouse;
import com.example.mrsapp.SearchByDateWarehouse.search_by_date_warehouse_adapter;
import com.example.mrsapp.TaskWarehouse.task_warehouse;
import com.example.mrsapp.TaskWarehouse.task_warehouse_adapter;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.IStatusListener;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;

public class MRSActivityWarehouse extends AppCompatActivity{

    //region GlobalVariables
    TextView tw;
    EditText curentDate;
    public ListView lv, lv2;
    ImageView online;
    public String fName, ins_spinner;
    public String inspector_now, tp_now;
    private int mYear, mMonth, mDay;
    //SearchableSpinner sp_inspector;
    //Spinner sp_tp, sp_inspector;
    ArrayList<String> listItems=new ArrayList<String>();
    ArrayList<task_warehouse> products = new ArrayList<task_warehouse>();
    ArrayList<search_by_date_warehouse> searchByDateWH = new ArrayList<>();
    ArrayAdapter<String> adapter;
    com.example.mrsapp.TaskWarehouse.task_warehouse_adapter task_warehouse_adapter;
    com.example.mrsapp.SearchByDateWarehouse.search_by_date_warehouse_adapter search_by_date_warehouse_adapter;
    private SharedPreferences mPrefs;
    private static final String PREFS_NAME = "PrefsFile";
    ArrayList yourlist;
    ArrayList yourlist1;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AutoCompleteTextView actv_tp, actv_inspector;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrs);



        mPrefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);



        //region Variables
        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);

        lv = findViewById(R.id.listview1);
        lv2 = findViewById(R.id.listview2);
        //sp_inspector = findViewById(R.id.spinner3);
        //sp_tp = findViewById(R.id.spinner2);
        online = findViewById(R.id.online);
        tw = findViewById(R.id.textView2);
        curentDate=(EditText) findViewById(R.id.date);
        actv_tp =  (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        actv_inspector =  (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        //endregion

        //region TabsConfig
        setTitle("TabHost");
        TabHost tabHost = findViewById(R.id.tabHost);
        tabHost.setup();
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Задачи");
        tabHost.addTab(tabSpec);
        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("История");
        tabHost.addTab(tabSpec);
        tabHost.setCurrentTab(0);
        //endregion

        SharedPreferences insp_tp = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
        actv_inspector.setText(insp_tp.getString("inspector_nm", ""));
        actv_tp.setText(insp_tp.getString("tp_this", ""));


        yourlist = null;
        yourlist1 = null;

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                products.clear();
                new GetFirstTenRecordsWarehouse(MRSActivityWarehouse.this).execute();
            }
        });

        //region CheckInternetConnectivity

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if(isOnline()){
                        online.setBackgroundResource(R.drawable.green_circle);
                    }else if(!isOnline()){
                        online.setBackgroundResource(R.drawable.red_circle);
                    }
                    handler.postDelayed(this, 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*try {
                    if(isConnected()){
                        online.setBackgroundResource(R.drawable.green_circle);
                    }else if(!isConnected()){
                        online.setBackgroundResource(R.drawable.red_circle);
                    }
                    handler.postDelayed(this, 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        };
        //Start
        handler.postDelayed(runnable, 1000);
        //endregion

        //region GetUsername
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA",Context.MODE_PRIVATE);
        fName = sharedPreferences.getString("fio",null);
        tw.setText(fName);
        //endregion


        //new GetAllTP(MRSActivityWarehouse.this).execute();
        new GetAllInspectors(MRSActivityWarehouse.this).execute();

        /*sp_inspector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                products.clear();
                new GetFirstTenRecordsWarehouse(MRSActivityWarehouse.this).execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });*/

        actv_inspector.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                products.clear();
                new GetFirstTenRecordsWarehouse(MRSActivityWarehouse.this).execute();
            }
        });

        actv_tp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                products.clear();
                new GetFirstTenRecordsWarehouse(MRSActivityWarehouse.this).execute();
            }
        });


        /*sp_tp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                products.clear();
                new GetFirstTenRecordsWarehouse(MRSActivityWarehouse.this).execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });*/

        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                search_by_date_warehouse tw = searchByDateWH.get(position);
                String meter_id = tw.getMeter_id();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
                sharedPreferences.edit().putString("meter_id",meter_id).apply();
                startActivity(new Intent(MRSActivityWarehouse.this, InfoViewActivity.class));
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                task_warehouse tw = products.get(position);
                String ids = tw.getId();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
                sharedPreferences.edit().putString("id",ids).apply();
                sharedPreferences.edit().putString("inspector_nm",actv_inspector.getText().toString()).apply();
                sharedPreferences.edit().putString("tp_this",actv_tp.getText().toString()).apply();
                sharedPreferences.edit().putString("wkeeper_nm",fName).apply();
                startActivity(new Intent(MRSActivityWarehouse.this, ScanActivity.class));
            }
        });

    }

    public boolean isConnected() throws InterruptedException, IOException {
        final String command = "ping -c 1 google.com";
        return Runtime.getRuntime().exec(command).waitFor() == 0;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /*public class GetAllTP extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public GetAllTP(MRSActivityWarehouse activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetAllTP();
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            yourlist = parseTP(result);
            //yourlist.add(0, "Выберите ТП...");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(MRSActivityWarehouse.this, android.R.layout.simple_spinner_item, yourlist);
            adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);//select_dialog_singlechoice
            /*Spinner spinner = (Spinner) findViewById(R.id.spinner2);
            spinner.setAdapter(adapter);
            spinner.setPrompt("Title");
            spinner.setSelection(0);
            ArrayAdapter<String> adapter_actv = new ArrayAdapter<String>
                    (MRSActivityWarehouse.this,android.R.layout.select_dialog_item,yourlist);
            actv_tp.setThreshold(1);
            actv_tp.setAdapter(adapter_actv);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }*/

    public class GetAllInspectors extends AsyncTask<String, String, String> {

        private ProgressDialog dialog;

        public GetAllInspectors(MRSActivityWarehouse activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.GetAllInspectors();
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            yourlist1 = parseTP(result);
            //yourlist1.add(0, "Выберите установщика...");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(MRSActivityWarehouse.this, android.R.layout.simple_spinner_item, yourlist1);
            adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);//select_dialog_singlechoice
            ArrayAdapter<String> adapter_actv = new ArrayAdapter<String>
                    (MRSActivityWarehouse.this,android.R.layout.select_dialog_item,yourlist1);
            actv_inspector.setThreshold(1);
            actv_inspector.setAdapter(adapter_actv);
            //sp_inspector.setAdapter(adapter);
            //sp_inspector.setSelection(0);

            //Spinner spinner = (Spinner) findViewById(R.id.spinner3);
            //spinner.setAdapter(adapter);
            //spinner.setPrompt("Title");
            //spinner.setSelection(0);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            ins_spinner = null;
        }
    }

    private ArrayList<String> parseTP(String tp) {
        final Pattern pattern = Pattern.compile("<string>(.+?)</string>");
        final Matcher matcher = pattern.matcher(tp);
        matcher.find();
        ArrayList<String> tp_list = new ArrayList<String>();
        while(matcher.find()){
            tp_list.add(matcher.group(1));
        }
        return tp_list;
    }

    private ArrayList<task_warehouse> parseList(String tp) {
        int i = 0;
        ArrayList<String> draft = new ArrayList<>();
        try {
            draft = parseArray(tp);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        for (i=0;i<draft.size();i++){
            StringTokenizer tokens = new StringTokenizer(draft.get(i), "|");
            String consumer = tokens.nextToken();// type
            String tp_name = tokens.nextToken();// name and surname
            String id = tokens.nextToken();// type
            String address = tokens.nextToken();
            products.add(new task_warehouse(consumer, tp_name,id, address));
        }
        return products;
    }

    private ArrayList<search_by_date_warehouse> parseListSearch(String tp){
        int i = 0;
        ArrayList<String> draft = new ArrayList<>();
        try {
            draft = parseArray(tp);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        for (i=0;i<draft.size();i++){
            StringTokenizer tokens = new StringTokenizer(draft.get(i), "|");
            String tp_name = tokens.nextToken();// type
            String consumer = tokens.nextToken();// name and surname
            String meter_id = tokens.nextToken();// type
            String date = tokens.nextToken();
            searchByDateWH.add(new search_by_date_warehouse(tp_name, consumer, meter_id, date ));
        }
        return searchByDateWH;
    }

    public void setDate(View v) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if(monthOfYear < 10 && dayOfMonth < 10){
                            searchByDateWH.clear();
                            curentDate.setText("0" + dayOfMonth + ".0" + (monthOfYear + 1) + "." + year);
                            new SearchRecordsByDateWarehouse(MRSActivityWarehouse.this).execute();
                        }else if(monthOfYear < 10 && dayOfMonth >= 10){
                            searchByDateWH.clear();
                            curentDate.setText(dayOfMonth + ".0" + (monthOfYear + 1) + "." + year);
                            new SearchRecordsByDateWarehouse(MRSActivityWarehouse.this).execute();
                        }else if(monthOfYear >= 10 && dayOfMonth < 10){
                            searchByDateWH.clear();
                            curentDate.setText("0" + dayOfMonth + "." + (monthOfYear + 1) + "." + year);
                            new SearchRecordsByDateWarehouse(MRSActivityWarehouse.this).execute();
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public class GetFirstTenRecordsWarehouse extends AsyncTask<String, String, String> {

        private ProgressDialog dialog;


        public GetFirstTenRecordsWarehouse(MRSActivityWarehouse activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            inspector_now = actv_inspector.getText().toString();
            tp_now = actv_tp.getText().toString();
            //tp_now = sp_tp.getSelectedItem().toString();
            //tp_now = sp_tp.getSelectedItem().toString();
            CallSoap cs = new CallSoap();
            String response = cs.GetFirstTenRecordsWarehouse(tp_now, inspector_now);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }else{
                ArrayList warehouse_records = new ArrayList();
                warehouse_records = parseList(result);
                task_warehouse_adapter = new task_warehouse_adapter(MRSActivityWarehouse.this, products);
                lv.setAdapter(task_warehouse_adapter);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public class SearchRecordsByDateWarehouse extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public SearchRecordsByDateWarehouse(MRSActivityWarehouse activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Загрузка данных...подождите");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response2 = cs.SearchRecordsByDateWarehouse(curentDate.getText().toString());
            return response2;
        }

        @Override
        protected void onPostExecute(String result2){
            super.onPostExecute(result2);
            parseListSearch(result2);
            if(result2.contains("Failed to connect")) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
            }else{
                search_by_date_warehouse_adapter = new search_by_date_warehouse_adapter(MRSActivityWarehouse.this, searchByDateWH);
                lv2.setAdapter(search_by_date_warehouse_adapter);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }
        return 0;
    }

    private ArrayList<String> parseArray(String tp) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(tp.getBytes()));
        final XPathExpression xPathExpression = XPathFactory.newInstance().newXPath().compile("//string/text()");
        final NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
        final ArrayList<String> tp_list = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            tp_list.add(nodeList.item(i).getNodeValue());
        }
        return tp_list;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}

