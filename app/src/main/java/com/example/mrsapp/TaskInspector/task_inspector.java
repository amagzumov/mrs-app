package com.example.mrsapp.TaskInspector;

public class task_inspector {
    public String tp;
    public String consumer;
    public String meter_id;
    public String id;

    public task_inspector(String _tp, String _consumer, String _meter_id, String _id) {
        tp = _tp;
        consumer = _consumer;
        meter_id = _meter_id;
        id = _id;
    }

    public String getId() {
        return ""+id;
    }
}
