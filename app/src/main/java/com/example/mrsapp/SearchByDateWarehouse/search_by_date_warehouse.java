package com.example.mrsapp.SearchByDateWarehouse;

public class search_by_date_warehouse {
    public String tp;
    public String consumer;
    public String meter_id;
    public String date;

    public search_by_date_warehouse(String _tp, String _consumer, String _meter_id, String _date) {
        tp = _tp;
        consumer = _consumer;
        meter_id = _meter_id;
        date = _date;
    }

    public String getMeter_id() {
        return ""+meter_id;
    }
}
