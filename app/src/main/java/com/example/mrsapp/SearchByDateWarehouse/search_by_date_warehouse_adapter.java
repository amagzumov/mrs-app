package com.example.mrsapp.SearchByDateWarehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mrsapp.R;

import java.util.ArrayList;

public class search_by_date_warehouse_adapter extends BaseAdapter {

    public Context ctx;
    public LayoutInflater lInflater;
    public ArrayList<search_by_date_warehouse> objects;

    public search_by_date_warehouse_adapter(Context context, ArrayList<search_by_date_warehouse> searchByDtWh) {
        ctx = context;
        objects = searchByDtWh;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_warehouse_search, parent, false);
        }

        search_by_date_warehouse p = getProduct(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        ((TextView) view.findViewById(R.id.tp)).setText(p.tp);
        ((TextView) view.findViewById(R.id.consumer)).setText(p.consumer);
        ((TextView) view.findViewById(R.id.meter_id)).setText(p.meter_id);

        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    search_by_date_warehouse getProduct(int position) {
        return ((search_by_date_warehouse) getItem(position));
    }
}
