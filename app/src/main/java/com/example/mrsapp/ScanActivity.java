package com.example.mrsapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import java.text.SimpleDateFormat;
import java.util.Date;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    //region GlobalVariables
    private ZXingScannerView zXingScannerView;
    private ImageView btn_flash, btn_camera;
    private String result_meter_id, type, id, fName, description;
    TextView user;
    ImageView online;
    ListView lv;
    private String curentDate;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        //region IdentifyElements
        online = findViewById(R.id.online);
        user = findViewById(R.id.user);
        lv = findViewById(R.id.listview1);
        btn_flash = findViewById(R.id.btn_flash);
        btn_camera = findViewById(R.id.btn_camera);
        //endregion


        //region StartScanner
        zXingScannerView = new ZXingScannerView(getApplicationContext());
        zXingScannerView = (ZXingScannerView) findViewById(R.id.zxscan);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
        //endregion

        //region CheckInternetConnectivity

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if(isOnline()){
                        online.setBackgroundResource(R.drawable.green_circle);
                    }else if(!isOnline()){
                        online.setBackgroundResource(R.drawable.red_circle);
                    }
                    handler.postDelayed(this, 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        //Start
        handler.postDelayed(runnable, 1000);
        //endregion

        //region SharedPrefs
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2",Context.MODE_PRIVATE);
        id = sharedPreferences.getString("id",null);

        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("DATA", Context.MODE_PRIVATE);
        type = sharedPreferences2.getString("type", null);
        fName = sharedPreferences2.getString("fio",null);
        user.setText(fName);
        //endregion

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date2 = new Date();
        curentDate = dateFormat.format(date2);

        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
        final int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        final boolean isCameraFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        //region FlashControl
        btn_flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isCameraFlash){
                    Toast.makeText(ScanActivity.this, "В телефоне отсутствует вспышка", Toast.LENGTH_SHORT).show();
                }else{
                    if(batLevel < 15){
                        Toast.makeText(ScanActivity.this, "Низкий уровень заряда батареи", Toast.LENGTH_SHORT).show();
                    }else{
                        zXingScannerView.setFlash(!zXingScannerView.getFlash());
                    }
                }

            }
        });
        //endregion

        //region CameraControl
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zXingScannerView.setResultHandler(ScanActivity.this);
                zXingScannerView.startCamera();
            }
        });
        //endregion
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onPause() {
        super.onPause();
        zXingScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        result_meter_id = result.toString();
        if(type.equals("warehouse")){
            new CheckInWarehouseMeters().execute();
        }else if(type.equals("inspector")) {
            if(MRSActivityInspector.undefined.equals("undefined")){
                new CheckInMetersUndefMeterPoint().execute();
            }else{
                new CheckInMeterExistAndReserved().execute();
            }
        }

    }

    //region CheckInWarehouseMeters and RecordToWarehouseMeters (Warehouse)
    public class CheckInWarehouseMeters extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.CheckInWarehouseMeters(result_meter_id);
            return response;
        }

        @SuppressLint("RestrictedApi")
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
                if(result.contains("true")){
                    Toast.makeText(ScanActivity.this, "Счетчик с таким id уже существует", Toast.LENGTH_SHORT).show();
                    zXingScannerView.resumeCameraPreview(ScanActivity.this);
                }else if(result.contains("false")){
                    final EditText txtUrl = new EditText(ScanActivity.this);
                    txtUrl.setSingleLine();
                    //txtUrl.setPadding(3, 0, 0, 0);
                    txtUrl.setText(result_meter_id);



                    new AlertDialog.Builder(ScanActivity.this)
                            .setTitle("Сообщение")
                            .setMessage("Номер счетчика")
                            .setView(txtUrl, 50, 0 ,0 ,0)
                            .setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    new RecordToWarehouseMeters().execute();
                                    zXingScannerView.resumeCameraPreview(ScanActivity.this);
                                }
                            })
                            .setNegativeButton("Пересканировать", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    zXingScannerView.resumeCameraPreview(ScanActivity.this);
                                    result_meter_id = "";
                                }
                            })
                            .show();
                }else if(result.contains("Failed to connect")){
                    Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
                    zXingScannerView.resumeCameraPreview(ScanActivity.this);
                }

        }
    }

    public class RecordToWarehouseMeters extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.RecordToWarehouseMeters(result_meter_id, fName, curentDate);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("success")){
                Toast.makeText(ScanActivity.this, "Счетчик сохранен", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("unsuccess")){
                Toast.makeText(ScanActivity.this, "Не удалось сохранить счетчик", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("Failed to connect")){
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }
        }
    }
    //endregion

    public class CheckInMetersUndefMeterPoint extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.CheckInMeterExistAndReserved(result_meter_id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("false2")){
                Toast.makeText(ScanActivity.this, "Счетчик уже присвоен абоненту", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("false")){
                Toast.makeText(ScanActivity.this, "Счетчик не был зарегистрирован в базе", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("true")){
                final EditText txtUrl = new EditText(ScanActivity.this);
                txtUrl.setText("");
                txtUrl.setHint("Введите адрес: ");


                new AlertDialog.Builder(ScanActivity.this)
                        .setTitle("Добавление точки учета")
                        .setMessage(result_meter_id)
                        .setView(txtUrl)
                        .setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                description = txtUrl.getText().toString();
                                new RecordToUndefinedMeteringPoint(ScanActivity.this).execute();
                                zXingScannerView.resumeCameraPreview(ScanActivity.this);
                            }
                        })
                        .setNegativeButton("Пересканировать", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                zXingScannerView.resumeCameraPreview(ScanActivity.this);
                                result_meter_id = "";
                            }
                        })
                        .show();
            }else if(result.contains("Failed to connect")){
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }


            /*if(result.contains("true")){
                Toast.makeText(ScanActivity.this, "Счетчик с таким id уже существует", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("false")){
                final EditText txtUrl = new EditText(ScanActivity.this);
                txtUrl.setText("");
                txtUrl.setHint("Введите адрес: ");


                new AlertDialog.Builder(ScanActivity.this)
                        .setTitle("Добавление точки учета")
                        .setMessage(result_meter_id)
                        .setView(txtUrl)
                        .setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                description = txtUrl.getText().toString();
                                new RecordToUndefinedMeteringPoint(ScanActivity.this).execute();
                                zXingScannerView.resumeCameraPreview(ScanActivity.this);
                            }
                        })
                        .setNegativeButton("Пересканировать", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                zXingScannerView.resumeCameraPreview(ScanActivity.this);
                                result_meter_id = "";
                            }
                        })
                        .show();
            }else if(result.contains("Failed to connect")){
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }*/

        }
    }

    //region CheckInMeterExistAndReserved and RecordToMetersWithUpdatingWarehouseMetersReservedStatus (Inspector)
    public class CheckInMeterExistAndReserved extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.CheckInMeterExistAndReserved(result_meter_id);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("false2")){
                Toast.makeText(ScanActivity.this, "Счетчик уже присвоен абоненту", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("false")){
                Toast.makeText(ScanActivity.this, "Счетчик не был зарегистрирован в базе", Toast.LENGTH_SHORT).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }else if(result.contains("true")){
                Toast.makeText(ScanActivity.this, "OK", Toast.LENGTH_SHORT).show();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA2", Context.MODE_PRIVATE);
                sharedPreferences.edit().putString("scanned_meter_id",result_meter_id).apply();
                startActivity(new Intent(ScanActivity.this, ScannedActivity.class));
            }else if(result.contains("Failed to connect")){
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером", Toast.LENGTH_LONG).show();
                zXingScannerView.resumeCameraPreview(ScanActivity.this);
            }

        }
    }
    //endregion

    public class RecordToUndefinedMeteringPoint extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public RecordToUndefinedMeteringPoint(ScanActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Сохранение данных...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            CallSoap cs = new CallSoap();
            String response = cs.RecordToUndefinedMeteringPoint(result_meter_id, description, curentDate, fName, MRSActivityInspector.selected_region);
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.contains("Failed to connect")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Ошибка соединения с сервером. Повторите попытку", Toast.LENGTH_LONG).show();
            }else if(result.contains("success")){
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if(type.equals("warehouse")){
                    Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ScanActivity.this, MRSActivityWarehouse.class));
                }else if(type.equals("inspector")){
                    Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ScanActivity.this, MRSActivityInspector.class));
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        if(type.equals("warehouse")){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }else if(type.equals("inspector")){
            MRSActivityInspector.undefined = "";
            MRSActivityInspector.selected_region = "";
            Intent intent = new Intent(getApplicationContext(), MRSActivityInspector.class);
            startActivity(intent);
        }
    }
}
